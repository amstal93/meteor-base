FROM ubuntu

ARG APP_BASE=meteor-base
ARG BUILD_BASE
ARG BUILD_DATE_BASE
ARG METEOR_SETTINGS_BASE
ARG PORT_BASE=3000
ARG VCS_REF_BASE
ARG VERSION_BASE


LABEL app=$APP_BASE license="GPLv3" maintainer="gitlab.com/Antmounds" org.label-schema.build-date=$BUILD_DATE_BASE org.label-schema.name="meteor-base" org.label-schema.description="ubuntu based starting point for developing in meteor" org.label-schema.url="http://antmounds.com" org.label-schema.vcs-url="https://gitlab.com/Antmounds/meteor-base" org.label-schema.vcs-ref=$VCS_REF_BASE org.label-schema.vendor="Antmounds" org.label-schema.version="0.9.0" org.label-schema.schema-version="1.0"

ENV APP=${APP_BASE} BUILD=$BUILD_BASE DEBIAN_FRONTEND=noninteractive PORT=$PORT_BASE ROOT_URL=http://localhost:$PORT_BASE NODE_ENV=production METEOR_SETTINGS=$METEOR_SETTINGS_BASE

RUN apt-get update && apt-get upgrade -y && apt-get install -y \
        apt-transport-https \
        apt-utils \
        aufs-tools \
        automake \
        build-essential \
        curl \
        g++ \
        gcc \
        git \
        lsb-release \
        make \
        ncdu \
        sudo \
        tree && \
    rm -rf /var/lib/apt/lists/* && \
    curl https://install.meteor.com/ | sh && \
    curl -sL https://deb.nodesource.com/setup_15.x | sh && \
    apt-get install -y nodejs && \  
    # Clean up stuff that's no longer needed
    apt-get autoremove -y && apt-get clean && \
    # Create user and add to sudoers
    useradd --create-home --shell /bin/bash nick && \
    echo 'nick ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers && \
    adduser nick sudo && \
    # Move .meteor to home folder to prevent re-downloading
    mv /root/.meteor /home/nick

USER nick

WORKDIR /home/nick/test

COPY .gitignore .

RUN sudo chown nick:nick -R /home/nick && \
    git clone https://github.com/meteor/localmarket && \
    cd /home/nick/test/localmarket/ && \
    meteor update && \
    meteor npm install --save --no-fund @babel/runtime && \
    npm audit fix && \
    cp ../.gitignore ./ && \
    git clean -Xdf

WORKDIR /home/nick/test/localmarket

EXPOSE 3000

ONBUILD WORKDIR /home/nick/app/src

ONBUILD COPY src/ .gitignore ./

ONBUILD RUN sudo mkdir ../build && \
            sudo chown -R nick:nick /home/nick/app && \
            meteor npm install --save --no-fund && \
            rm -R /home/nick/test && \
            git clean -Xdf

CMD [ "meteor" ]